terraform {
backend "gcs" {
  bucket = "aditya-test-bucket-demo" //Give your bucket name
  prefix = "terraform/tfstat"
  credentials = "keyfile.json"
  }
}
